# UF Header

This plugin generates and updates UNIT Factory header.
It can be used with any language, but was tested with C/C++, Makefiles, and python files.

It's capable of:

* Generating valid UNIT Factory header and inserting it into the file.
* Updating the header on save automatically.


Use ``` Tools -> Enter UF Login... ``` to enter your login(and email).
Use ``` Tools -> Insert UF Header ``` (Alt + H) to insert or update the header.

All JetBrains IDEs built in 2017 and later are supported.

## This plugin is deprecated. 

**There is no need to use it anymore as UNIT Factory closed in 2020.**

## Manual Installing

Download UF Header.jar, go to your JetBrains IDE and use
```Preferences -> Plugins -> Install plugin from disk...``` and select UFHeader.jar(you may need to restart the IDE after this).

Jar file can be deleted after restart.

## Using

Use ```Tools -> Enter UF Login...``` to edit your UNIT Factory login(and email).

If you specify only your login:

![screenshot1](screenshots/onlylogin.png)

Then the header will look like:

![header1](screenshots/onlyloginheader.png)

Or, if you specify your full email:

![screenshot2](screenshots/fullemail.png)

Then the header will look like:

![header2](screenshots/fullemailheader.png)

Your credentials are saved between IDE reload, so once entered they will always be present.

Use ```Tools -> Insert UF Header``` (Alt + H) to insert the header into the current file.
The plugin will automatically update the header when saving the file, if the header is present.


## License
![gplv3-88x31.png](https://www.gnu.org/graphics/gplv3-88x31.png)

This project is licensed under the GNU GPL License, Version 3 - see [LICENSE.md](LICENSE.md) for details.

## Contributing/Issue creating

Raise an issue in the issue tracker, or write me a letter.

## Contacts

* [Bitbucket](https://bitbucket.org/Mitriksicilian/)
* [E-mail](mailto:MitrikSicilian@icloud.com?subject=UF Header from Bitbucket)
* MitrikSicilian@icloud.com

## UNIT Factory
![UNIT Factory logo](https://unit.ua/static/img/logo.png)

[UNIT Factory](https://uk.wikipedia.org/wiki/UNIT_Factory) was an innovative programming school and a part of [School 42](https://en.wikipedia.org/wiki/42_(school)) in [the heart](https://en.wikipedia.org/wiki/Kiev) of [Ukraine](https://en.wikipedia.org/wiki/Ukraine).
