package ua.dkovalch.header;

/*
 **   This file is part of UFHeader project.
 **   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
 **
 **   This program is free software: you can redistribute it and/or modify
 **   it under the terms of the GNU General Public License as published by
 **   the Free Software Foundation, version 3 of the License.
 **
 **   This program is distributed in the hope that it will be useful,
 **   but WITHOUT ANY WARRANTY; without even the implied warranty of
 **   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 **   GNU General Public License for more details.
 **
 **   You should have received a copy of the GNU General Public License
 **   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import com.intellij.ide.util.PropertiesComponent;

/**
 * This class saves and retrieves user login and email.
 */
class Storage {
    private static Storage ourInstance = new Storage();
    final String defaultEmail = "marvin@42.fr";
    private String login;
    private String email;

    private Storage() {
        login = PropertiesComponent.getInstance().getValue(
                "ua.dkovalch.UFHeader.login", System.getenv("USER"));
        email = PropertiesComponent.getInstance().getValue(
                "ua.dkovalch.UFHeader.email", defaultEmail);
    }

    static Storage getInstance() {
        return ourInstance;
    }

    String getEmail() {
        return email;
    }

    String getLogin() {
        return login;
    }

    String getData() {
        return email.equals(defaultEmail) ? login : email;
    }

    void setData(String data) {
        if (data.contains("@")) {
            email = data;
            login = data.substring(0, data.indexOf('@'));
        } else {
            email = defaultEmail;
            login = data;
        }
        PropertiesComponent.getInstance().setValue(
                "ua.dkovalch.UFHeader.login", login);
        PropertiesComponent.getInstance().setValue(
                "ua.dkovalch.UFHeader.email", email);
    }
}
