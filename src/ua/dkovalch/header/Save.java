package ua.dkovalch.header;

/*
 **   This file is part of UFHeader project.
 **   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
 **
 **   This program is free software: you can redistribute it and/or modify
 **   it under the terms of the GNU General Public License as published by
 **   the Free Software Foundation, version 3 of the License.
 **
 **   This program is distributed in the hope that it will be useful,
 **   but WITHOUT ANY WARRANTY; without even the implied warranty of
 **   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 **   GNU General Public License for more details.
 **
 **   You should have received a copy of the GNU General Public License
 **   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import com.intellij.notification.NotificationType;
import com.intellij.notification.Notifications;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileEvent;
import com.intellij.openapi.vfs.VirtualFileListener;
import com.intellij.openapi.vfs.VirtualFileManager;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import org.jetbrains.annotations.NotNull;

public class Save implements ApplicationComponent {
    public Save() {
    }

    /**
     * This is the place where update on save happens.
     */
    @Override
    public void initComponent() {
        VirtualFileManager.getInstance().addVirtualFileListener(new VirtualFileListener() {
            public void contentsChanged(@NotNull VirtualFileEvent event) {
                final VirtualFile file = event.getFile();
                final Document document = FileDocumentManager.getInstance().getDocument(file);

                if (document == null) {
                    //Not notifying about this, because it happens pretty often on start-up.
                    return;
                }

                Header headerGen = new Header(file.getName());
                final FileDocumentManager docManager = FileDocumentManager.getInstance();
                if (!headerGen.isPresent(document.getCharsSequence().toString())
                        || !docManager.isDocumentUnsaved(document)) {
                    //Nothing to do here if there is no header, or if there are no unsaved changes.
                    return;
                }

                if (!file.isWritable()) {
                    document.fireReadOnlyModificationAttempt();
                    Notifications.Bus.notify(new MyNotification("Cannot insert or update header",
                            "The file is not writable.", NotificationType.INFORMATION));
                    return;
                }

                //Locating file in all of the open projects
                Project currentProject = null;
                final Project[] projects = ProjectManager.getInstance().getOpenProjects();
                for (final Project project : projects) {
                    currentProject = project;
                    PsiFile result = PsiManager.getInstance(currentProject).findFile(file);
                    if (result != null)
                        break;
                }

                if (currentProject == null) {
                    Notifications.Bus.notify(new MyNotification("The project is null",
                            "Click on the editor and try again.", NotificationType.ERROR));
                    return;
                }

                final String updated = headerGen.getUpdatedLine();
                final String filename = headerGen.getFilenameLine();
                final String lastAuthor = headerGen.getAuthorLine();
                final int start = headerGen.getPosition(document.getCharsSequence().toString());

                WriteCommandAction.runWriteCommandAction(currentProject, () -> {
                    //We need to re-paste filename in case of renaming the file.
                    document.replaceString(start + 243, start + 243 + filename.length(), filename);
                    document.replaceString(start + 405, start + 405 + lastAuthor.length(), lastAuthor);
                    document.replaceString(start + 648, start + 648 + updated.length(), updated);
                });
            }
        });
    }

    @Override
    public void disposeComponent() {
    }

    @Override
    @NotNull
    public String getComponentName() {
        return "Save";
    }
}
