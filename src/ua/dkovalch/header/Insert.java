package ua.dkovalch.header;

/*
 **   This file is part of UFHeader project.
 **   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
 **
 **   This program is free software: you can redistribute it and/or modify
 **   it under the terms of the GNU General Public License as published by
 **   the Free Software Foundation, version 3 of the License.
 **
 **   This program is distributed in the hope that it will be useful,
 **   but WITHOUT ANY WARRANTY; without even the implied warranty of
 **   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 **   GNU General Public License for more details.
 **
 **   You should have received a copy of the GNU General Public License
 **   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import com.intellij.notification.NotificationType;
import com.intellij.notification.Notifications;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;

public class Insert extends AnAction {
    /**
     * This function actually does the job upon pressing "Insert"
     *
     * @param AnActionEvent
     */
    @Override
    public void actionPerformed(AnActionEvent AnActionEvent) {
        final VirtualFile file = AnActionEvent.getData(CommonDataKeys.VIRTUAL_FILE);
        final Editor editor = AnActionEvent.getData(CommonDataKeys.EDITOR);

        if (file == null || editor == null) {
            Notifications.Bus.notify(new MyNotification(
                            "Cannot insert or update header",
                            "Document or editor is null. Click on the editor and try again.",
                            NotificationType.ERROR),
                    AnActionEvent.getProject());
            return;
        }
        if (!file.isWritable()) {
            editor.getDocument().fireReadOnlyModificationAttempt();
            Notifications.Bus.notify(new MyNotification("Cannot insert or update header",
                            "The file is not writable.", NotificationType.INFORMATION),
                    AnActionEvent.getProject());
            return;
        }

        Header headerGen = new Header(file.getName());
        Runnable theAction;

        if (headerGen.isPresent(editor.getDocument().getText())) {
            final String updated = headerGen.getUpdatedLine();
            final String filename = headerGen.getFilenameLine();
            final String lastAuthor = headerGen.getAuthorLine();
            final int start = headerGen.getPosition(editor.getDocument().getText());

            theAction = () -> {
                //We need to re-paste filename in case of renaming the file.
                editor.getDocument().replaceString(start + 243, start + 243 + filename.length(), filename);
                editor.getDocument().replaceString(start + 405, start + 405 + lastAuthor.length(), lastAuthor);
                editor.getDocument().replaceString(start + 648, start + 648 + updated.length(), updated);
            };
        } else {
            final String header = headerGen.toString();

            theAction = () -> editor.getDocument().insertString(0, header);
        }

        WriteCommandAction.runWriteCommandAction(AnActionEvent.getProject(), theAction);
//        if (ScrollSettingsEnabled) { //Lets scroll it a bit
//            ScrollingModel scrollingModel = editor.getScrollingModel();
//            scrollingModel.scrollVertically(scrollingModel.getVerticalScrollOffset() - 11);
//        }
    }

    /**
     * This disables "Insert" menu item when we are not in the project and editor.
     *
     * @param anActionEvent
     */
    @Override
    public void update(final AnActionEvent anActionEvent) {
        final Project project = anActionEvent.getData(CommonDataKeys.PROJECT);
        final Editor editor = anActionEvent.getData(CommonDataKeys.EDITOR);
        anActionEvent.getPresentation().setVisible((project != null && editor != null));
    }
}
