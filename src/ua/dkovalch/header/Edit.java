package ua.dkovalch.header;

/*
 **   This file is part of UFHeader project.
 **   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
 **
 **   This program is free software: you can redistribute it and/or modify
 **   it under the terms of the GNU General Public License as published by
 **   the Free Software Foundation, version 3 of the License.
 **
 **   This program is distributed in the hope that it will be useful,
 **   but WITHOUT ANY WARRANTY; without even the implied warranty of
 **   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 **   GNU General Public License for more details.
 **
 **   You should have received a copy of the GNU General Public License
 **   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import com.intellij.notification.NotificationType;
import com.intellij.notification.Notifications;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.ui.InputValidator;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.util.IconLoader;

public class Edit extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent e) {
        Storage storage = Storage.getInstance();
        Messages.InputDialog dialog = new Messages.InputDialog(e.getProject(),
                "Enter UNIT Factory login,\nor full email, if you want to override the default one: " + storage.defaultEmail,
                "Enter UNIT Factory Login", IconLoader.getIcon("/icons/UFLogo.png"), storage.getData(),
                new InputValidator() {
                    @Override
                    public boolean checkInput(String entry) {
                        return entry != null && !entry.isEmpty();
                    }

                    @Override
                    public boolean canClose(String entry) {
                        return checkInput(entry);
                    }
                });

        dialog.centerRelativeToParent();
        if (!dialog.showAndGet()) {
            //Not OK? Nothing to do here anymore then.
            return;
        }

        final String input = dialog.getInputString();
        if (input != null) {//It will not be null, but anyway
            storage.setData(input);

            Notifications.Bus.notify(new MyNotification("Updated login",
                    "Login: " + storage.getLogin() + ". Email: " + storage.getEmail() + ".",
                    NotificationType.INFORMATION));
        } else {
            Notifications.Bus.notify(new MyNotification("Oops",
                    "Your login has not been updated.",
                    NotificationType.WARNING));
        }
    }
}
