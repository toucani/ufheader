package ua.dkovalch.header;

/*
 **   This file is part of UFHeader project.
 **   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
 **
 **   This program is free software: you can redistribute it and/or modify
 **   it under the terms of the GNU General Public License as published by
 **   the Free Software Foundation, version 3 of the License.
 **
 **   This program is distributed in the hope that it will be useful,
 **   but WITHOUT ANY WARRANTY; without even the implied warranty of
 **   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 **   GNU General Public License for more details.
 **
 **   You should have received a copy of the GNU General Public License
 **   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Date;

class Header {
    private final String _filename;
    private final String _topLine;
    private final String _emptyLine;
    private final String _edge;
    private String _header = "";

    Header(@NotNull String filename) {
        _filename = filename;
        _edge = getEdge();
        _topLine = getLine(true);
        _emptyLine = getLine(false);
    }

    /**
     * Returns the position of the first line of the header.
     *
     * @param fileContents contents of the file
     * @return 0-based position of the first line
     */
    int getPosition(@NotNull final String fileContents) {
        return fileContents.indexOf(_topLine);
    }

    /**
     * Checks whether there already is a header in the given file.
     * Note that the file we created Header with must be the same file
     * which contents is passed here.
     *
     * @param fileContents contents of the file.
     * @return true if the header is present.
     */
    boolean isPresent(@NotNull final String fileContents) {
        if (fileContents.length() < 891)//Header's length is 890 + \n
            return false;

        final int start = getPosition(fileContents);
        if (start < 0 || fileContents.length() - start < 891)
            return false;

        final String headerContents = fileContents.substring(start, start + 891);
        final int length = _topLine.length();

        return headerContents.indexOf(_topLine) == 0
                && headerContents.indexOf(_emptyLine) == 81
                && _emptyLine.equals(headerContents.substring(729, 729 + length))
                && _topLine.equals(headerContents.substring(810, 810 + length));
    }

    /**
     * Creates full line with filename.
     *
     * @return full line with filename and part of 42.
     */
    String getFilenameLine() {
        String name = _filename;

        if (name.length() > 50) //It will overlap with the 42 if too long.
            name = name.substring(0, 49);

        StringBuilder filename = new StringBuilder(_emptyLine);

        filename.replace(5, 5 + name.length(), name);
        filename.replace(56, 75, ":+:      :+:    :+:");

        return filename.toString();
    }

    /**
     * Creates full line with author and email.
     *
     * @return full line with author and email.
     */
    String getAuthorLine() {
        String result = "By: " + Storage.getInstance().getLogin() + " <" + Storage.getInstance().getEmail() + ">";

        if (result.length() > 44)//It will overlap with 42 if too long
            result = result.substring(0, 43);

        StringBuilder line = new StringBuilder(_emptyLine);

        line.replace(5, 5 + result.length(), result);
        line.replace(52, 70, "+#+  +:+       +#+");

        return line.toString();
    }

    /**
     * Creates a line with date and time.
     *
     * @return date and time like: yyyy/MM/dd HH:mm:ss
     */
    private String getDateTime() {
        return new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
    }

    /**
     * Creates full line with author and date.
     *
     * @return full line with author and date.
     */
    private String getCreatedLine() {
        final String date = getDateTime();
        String result = "Created: " + date + " by " + Storage.getInstance().getLogin();

        if (result.length() > 47)
            result = result.substring(0, 46);

        StringBuilder line = new StringBuilder(_emptyLine);

        line.replace(5, 5 + result.length(), result);
        line.replace(55, 65, "#+#    #+#");

        return line.toString();
    }

    /**
     * Creates full line with update author and date.
     *
     * @return full line with update author and date.
     */
    String getUpdatedLine() {
        final String date = getDateTime();
        String result = "Updated: " + date + " by " + Storage.getInstance().getLogin();

        if (result.length() > 47)
            result = result.substring(0, 46);

        StringBuilder line = new StringBuilder(_emptyLine);

        line.replace(5, 5 + result.length(), result);
        line.replace(54, 71, "###   ########.fr");

        return line.toString();
    }

    /**
     * Returns fully generated header as a string.
     *
     * @return the whole header in a one string.
     */
    public String toString() {
        if (_header.isEmpty()) {
            StringBuilder header = new StringBuilder(891);

            //Creating the table
            header.append(_topLine);
            header.append(_emptyLine);
            header.append(_emptyLine);
            header.append(getFilenameLine());
            header.append(_emptyLine);
            header.append(getAuthorLine());
            header.append(_emptyLine);
            header.append(getCreatedLine());
            header.append(getUpdatedLine());
            header.append(_emptyLine);
            header.append(_topLine);
            header.append('\n');

            //Engrave 42 symbol
            //If you're changing this, be very careful with these magic numbers.
            //And also don't to forget to update this:
            //  hours wasted to debug the numbers: 2
            header.replace(220, 237, ":::      ::::::::");
//            header.replace(299, 318, ":+:      :+:    :+:");
            header.replace(378, 397, "+:+ +:+         +:+");
//            header.replace(457, 475, "+#+  +:+       +#+");
            header.replace(536, 553, "+#+#+#+#+#+   +#+");
//            header.replace(622, 632, "#+#    #+#");
//            header.replace(702, 719, "###   ########.fr");

            _header = header.toString();
        }

        return _header;
    }

    /**
     * Returns valid header start/end sequence
     *
     * @return "# " in case we are working with Makefile, "/* " otherwise
     */
    private String getEdge() {
        return (_filename.endsWith(".c") || _filename.endsWith(".cpp")
                || _filename.endsWith(".h") || _filename.endsWith(".hpp")
                || _filename.endsWith(".java"))
                ? "/* " : "# *";
    }

    /**
     * Builds header line with newline in the end.
     *
     * @param top fills the line with * if true
     * @return full line
     */
    private String getLine(final boolean top) {
        StringBuilder builder = new StringBuilder(_edge);
        final String last = builder.reverse().toString();
        builder.reverse();
        builder.append(top
                ? "**************************************************************************"
                : "                                                                          ");
        builder.append(last);
        builder.append('\n');
        if (!top && _edge.charAt(0) == '#') {
            //This fixes additional * in case of Makefile
            builder.setCharAt(2, ' ');
            builder.setCharAt(77, ' ');
        }

        return builder.toString();
    }
}
